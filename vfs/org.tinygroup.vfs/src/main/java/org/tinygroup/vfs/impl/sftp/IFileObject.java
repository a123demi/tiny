package org.tinygroup.vfs.impl.sftp;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import org.tinygroup.vfs.FileObject;

public interface IFileObject extends FileObject {
	/**
	 * 上传资源
	 * 
	 * @param 可以是流
	 *            、文件、文件路径(上传位置不存在时会自动创建)
	 * @exception 上传资源不存在或获取出错时会抛出运行时异常
	 *                ，上传位置为文件夹时也会抛出运行时异常
	 */
	public void upload(InputStream in);

	public void upload(String sourceFile);

	public void upload(File sourceFile);

	/**
	 * 下载资源
	 * 
	 * @param 可以是输出到流
	 *            、文件、文件路径
	 * @exception 输出资源不存在或获取出错时会抛出运行时异常
	 *                ，下载资源为文件夹时也会抛出运行时异常
	 */
	public void download(OutputStream out);

	public void download(String dstFile);

	public void download(File dstFile);
}
