package org.tinygroup.vfs.impl.sftp;

import org.tinygroup.vfs.FileObject;
import org.tinygroup.vfs.SchemaProvider;

/**
 * Created by Mick Woo on 2016年8月23日下午4:50:51
 */
public class SftpSchemaProvider implements SchemaProvider {
	public static final String SFTP_PROTOCAL = "sftp:";

	public boolean isMatch(String resource) {
		return resource.toLowerCase().startsWith(SFTP_PROTOCAL);
	}

	public String getSchema() {
		return SFTP_PROTOCAL;
	}

	public FileObject resolver(String resource) {
		return new SftpFileObject(this, resource);
	}

}
