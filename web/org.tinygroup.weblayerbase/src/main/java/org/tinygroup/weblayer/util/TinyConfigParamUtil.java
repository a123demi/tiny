package org.tinygroup.weblayer.util;

import java.util.HashMap;
import java.util.Map;

import org.tinygroup.remoteconfig.RemoteConfigReadClient;

public class TinyConfigParamUtil {
	public final static String REMOTE_CONFIG_CLASS = "remote-config-class";

	private static Map<String, String> map = new HashMap<String, String>();
	
	private static RemoteConfigReadClient readClient;

	public static RemoteConfigReadClient getReadClient() {
		return readClient;
	}

	public static void setReadClient(RemoteConfigReadClient readClient) {
		TinyConfigParamUtil.readClient = readClient;
	}

	public static void putConfig(String param, String value) {
		map.put(param, value);
	}

	public static void removeConfig(String param) {
		map.remove(param);
	}

	public static String getConfig(String name) {
		return map.get(name);
	}

	public static void clear() {
		map.clear();
		readClient = null;
	}
}
