package org.tinygroup.weblayer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 过滤器处理接口
 * 
 * @author renhui
 *
 */
public interface FilterHandler {
	/**
	 * 获取处理上下文
	 * 
	 * @return
	 */
	public WebContext getContext();

	/**
	 * 处理方法
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	public void tinyFilterProcessor(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException;

	/**
	 * 头部提交，保证内容只提交一次
	 *
	 * @param wrapperedContext
	 */
	public void commitHeaders(WebContext wrapperedContext);
    
	/**
	 * 返回请求路径
	 * @return
	 */
	public String getServletPath();

}
