package org.tinygroup.cepcoreimpl.test.newcase.testcase;

import java.util.ArrayList;
import java.util.List;

import org.tinygroup.cepcore.CEPCore;
import org.tinygroup.cepcore.EventProcessor;
import org.tinygroup.cepcoreimpl.test.newcase.VirtualEventProcesor;
import org.tinygroup.cepcoreimpl.test.newcase.VirtualService;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.event.ServiceInfo;

import junit.framework.TestCase;

public class BaseDataCreate extends TestCase{
	protected List<ServiceInfo> getService(String ids) {
        List<ServiceInfo> list = new ArrayList<ServiceInfo>();
        String[] service = ids.split(",");
        for (String id : service) {
            VirtualService v = new VirtualService(id);
            list.add(v);
        }
        return list;
    }

	protected EventProcessor getEventProcessor(String id, int type, String ids) {
        VirtualEventProcesor eventProcessor = new VirtualEventProcesor();
        eventProcessor.setId(id);
        eventProcessor.getServiceInfos().addAll(getService(ids));
        eventProcessor.setType(type);
        return eventProcessor;
    }

	protected List<String> trans(String str) {

        List<String> list = new ArrayList<String>();
        if (StringUtil.isBlank(str)) {
            return list;
        }
        String[] array = str.split(",");
        for (String s : array) {
            list.add(s);
        }
        return list;
    }

    protected void registerEventProcessor(CEPCore cepcore, String id, int type,
                                          String ids) {
        EventProcessor processorLocal = getEventProcessor(id, type, ids);
        cepcore.registerEventProcessor(processorLocal);
    }

    protected void unRegisterEventProcessor(CEPCore cepcore, String id,
                                            int type, String ids) {
        EventProcessor processorRemote = getEventProcessor(id, type, ids);
        cepcore.unregisterEventProcessor(processorRemote);
    }

}
