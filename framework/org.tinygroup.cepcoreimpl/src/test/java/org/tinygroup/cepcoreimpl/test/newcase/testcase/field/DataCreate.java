package org.tinygroup.cepcoreimpl.test.newcase.testcase.field;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tinygroup.cepcore.CEPCore;
import org.tinygroup.cepcore.EventProcessor;
import org.tinygroup.cepcoreimpl.test.newcase.testcase.BaseDataCreate;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.event.ServiceInfo;

public class DataCreate extends BaseDataCreate {
	protected Map<String, List<EventProcessor>> createServiceIdMap(
			CEPCore cepcore) {
		return new HashMap<String, List<EventProcessor>>();
	}
	

	protected Map<String, EventProcessor> createProcessorMap() {
		return new HashMap<String, EventProcessor>();
	}
	
	protected Map<String, EventProcessor> addProcessorMap(Map<String, EventProcessor> map,String processorId,int type,String ids) {
		EventProcessor processor = getEventProcessor(processorId, type, ids);
		map.put(processorId, processor);
		return map;
	}

	protected Map<String, ServiceInfo> createLocalServiceMap(String ids) {
		List<ServiceInfo> list = createLocalServices(ids);
		Map<String, ServiceInfo> map = new HashMap<String, ServiceInfo>();
		for (ServiceInfo info : list) {
			map.put(info.getServiceId(), info);
		}
		return map;
	}

	protected Map<String, ServiceInfo> createRemoteServiceMap(String ids) {
		Map<String, ServiceInfo> map = new HashMap<String, ServiceInfo>();
		if(StringUtil.isBlank(ids)){
			return map;
		}
		List<ServiceInfo> list = getService(ids);
		
		for (ServiceInfo info : list) {
			map.put(info.getServiceId(), info);
		}
		return map;
	}

	protected List<ServiceInfo> createLocalServices(String ids) {
		if(StringUtil.isBlank(ids)){
			return new ArrayList<ServiceInfo>();
		}
		return getService(ids);
	}

	protected Map<String, List<ServiceInfo>> createEventProcessorServices() {
		return new HashMap<String, List<ServiceInfo>>();
	}

	protected Map<String, List<ServiceInfo>> addEventProcessorServices(
			Map<String, List<ServiceInfo>> map, String eventProcessorId, 
			String ids) {
		if(StringUtil.isBlank(eventProcessorId)){
			return map;
		}
		map.put(eventProcessorId, getService(ids));
		return map;
	}

}
