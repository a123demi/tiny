package org.tinygroup.cepcoreimpl.test.newcase.threadlocal;

import org.tinygroup.cepcore.util.ThreadContextUtil;
import org.tinygroup.cepcoreimpl.test.testcase.CEPCoreBaseTestCase;
import org.tinygroup.context.util.ContextFactory;
import org.tinygroup.event.Event;

public class TestThreadLocalService extends CEPCoreBaseTestCase {
	public void testAsyn() {
		ThreadContextUtil.put("a", "a");
		Event event = getEvent(Event.EVENT_MODE_ASYNCHRONOUS);
		getCore().process(event);
		assertEquals("a", ThreadContextUtil.get("a"));
	}

	public void testSyn() {
		ThreadContextUtil.put("a", "a");
		Event event = getEvent(Event.EVENT_MODE_SYNCHRONOUS);
		getCore().process(event);
		assertEquals("a", ThreadContextUtil.get("a"));
	}

	private Event getEvent(int mode) {
		Event e = Event.createEvent("threadLocalService",
				ContextFactory.getContext());
		e.setMode(mode);
		return e;
	}
}
