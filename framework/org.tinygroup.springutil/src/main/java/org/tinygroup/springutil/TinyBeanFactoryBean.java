package org.tinygroup.springutil;

import org.springframework.beans.factory.FactoryBean;
import org.tinygroup.beancontainer.BeanContainerFactory;

/**
 * 通过FactoryBean方式获取tiny容器加载的bean对象
 * @author renhui
 *
 */
public class TinyBeanFactoryBean implements FactoryBean {

	private Class objectType;

	private boolean isSingleton = true;

	public void setObjectType(String objectType) {
		try {
			this.objectType = Class.forName(objectType);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public void setSingleton(boolean isSingleton) {
		this.isSingleton = isSingleton;
	}

	public Object getObject() throws Exception {
		return BeanContainerFactory.getBeanContainer(
				getClass().getClassLoader()).getBean(objectType);
	}

	public Class getObjectType() {
		return objectType;
	}

	public boolean isSingleton() {
		return isSingleton;
	}
}
