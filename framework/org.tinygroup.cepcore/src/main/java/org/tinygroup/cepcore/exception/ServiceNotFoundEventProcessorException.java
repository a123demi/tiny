package org.tinygroup.cepcore.exception;

import org.tinygroup.cepcoreexceptioncode.CEPCoreExceptionCode;
import org.tinygroup.exception.BaseRuntimeException;

public class ServiceNotFoundEventProcessorException  extends BaseRuntimeException{
	  /**
	 * 
	 */
	private static final long serialVersionUID = -4502636155637066681L;
	private final String serviceId;

	public ServiceNotFoundEventProcessorException(String serviceId) {
        super(CEPCoreExceptionCode.SERVICE_NOT_FOUND_EVENTPROCESSOR_EXCEPTION_CODE, serviceId);
        this.serviceId = serviceId;
    }
	 public String getServiceId() {
	        return serviceId;
	    }

}
