package org.tinygroup.commons.tools;

import java.util.Map;
import java.util.Properties;

public class VariableUtil {
	
	/**
	 * 获取环境变量
	 * @param key
	 * @return
	 */
	public static String getEnvVariable(String key){
		Map<String,String> map = System.getenv();
		return map.get(key);
	}
	
	/**
	 * 获取系统变量
	 * @param key
	 * @return
	 */
	public static Object getSysVariable(String key){
		Properties p = System.getProperties();
		return p.get(key);
	}
}