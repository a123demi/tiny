package org.tinygroup.tinyrunner.extended;

import org.springframework.context.ApplicationContext;
import org.tinygroup.springutil.SpringBeanContainer;

public class ExtendedSpringBeanContainer extends SpringBeanContainer {
	private ApplicationContext applicationContext = null;

	public ExtendedSpringBeanContainer(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public ApplicationContext getBeanContainerPrototype() {
		if(applicationContext==null){
			super.getBeanContainerPrototype();
		}
		return applicationContext;
	}

	public ExtendedSpringBeanContainer(ExtendedSpringBeanContainer parent,
			ClassLoader loader) {
		super(parent, loader);
	}
}
