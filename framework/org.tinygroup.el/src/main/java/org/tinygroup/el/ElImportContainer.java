package org.tinygroup.el;

import java.util.HashMap;
import java.util.Map;

public class ElImportContainer {
	private static Map<String, Object> imports = new HashMap<String, Object>();
	
	public static void addImport(String key,Object importTarget){
		imports.put(key, importTarget);
	}
	
	public static void remoteImport(String key){
		imports.remove(key);
	}
	
	public static void clear(){
		imports.clear();
	}
	
	public static Map<String, Object> getImports(){
		return imports;
	}
}
