package org.tinygroup.nettyremote.codec.serialization;

import com.caucho.hessian.io.SerializerFactory;

public class SerializerFactoryUtil {
	private static SerializerFactory serializerFactory;
	static{
		serializerFactory = new SerializerFactory();
        serializerFactory.addFactory(new BigDecimalSerializerFactory());
	}
	
	private SerializerFactoryUtil(){}
	
	public static SerializerFactory getSerializerFactory(){
		return serializerFactory;
	}
}
