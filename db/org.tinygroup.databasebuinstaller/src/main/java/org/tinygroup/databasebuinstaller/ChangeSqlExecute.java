package org.tinygroup.databasebuinstaller;

import org.tinygroup.exception.BaseRuntimeException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * 执行增量sql的类
 * 
 * @author wangwy
 *
 */
public class ChangeSqlExecute {

	private DatabaseInstallerProcessor databaseInstallerProcessor;

	public void setDatabaseInstallerProcessor(
			DatabaseInstallerProcessor databaseInstallerProcessor) {
		this.databaseInstallerProcessor = databaseInstallerProcessor;
	}

	public void sqlExecute() {
		DataSource dataSource = DataSourceHolder.getDataSource();
		Connection con = null;
		try {
			List<String> toolChangeSqls = databaseInstallerProcessor.getChangeSqls();
			con = dataSource.getConnection();
			databaseInstallerProcessor.execute(toolChangeSqls, con);
		} catch (SQLException ex) {
			throw new BaseRuntimeException(ex);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}

	}

}
