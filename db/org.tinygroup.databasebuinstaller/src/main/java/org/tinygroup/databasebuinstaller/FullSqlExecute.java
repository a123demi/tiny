package org.tinygroup.databasebuinstaller;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.tinygroup.exception.BaseRuntimeException;

/**
 * 执行全量sql的类
 * 
 * @author renhui
 *
 */
public class FullSqlExecute {

	private DatabaseInstallerProcessor databaseInstallerProcessor;

	public void setDatabaseInstallerProcessor(
			DatabaseInstallerProcessor databaseInstallerProcessor) {
		this.databaseInstallerProcessor = databaseInstallerProcessor;
	}

	public void sqlExecute() {
		DataSource dataSource = DataSourceHolder.getDataSource();
		Connection con = null;
		try {
			List<String> toolFullSqls = databaseInstallerProcessor
					.getFullSqls();
			con = dataSource.getConnection();
			databaseInstallerProcessor.execute(toolFullSqls, con);
		} catch (SQLException ex) {
			throw new BaseRuntimeException(ex);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}

	}

}
