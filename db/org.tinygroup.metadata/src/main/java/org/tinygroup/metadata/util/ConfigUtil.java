package org.tinygroup.metadata.util;

/**
 * Created by wangwy11342 on 2016/8/24.
 */
public class ConfigUtil {
    private static boolean isCheckStrict = true;
    private static boolean initDataDel = false;
    private static boolean isCheckModified = true;

    public static void setIsCheckStrict(boolean isCheckStrict) {
        ConfigUtil.isCheckStrict = isCheckStrict;
    }

    public static void setInitDataDel(boolean isInitDataDel){
            initDataDel = isInitDataDel;
    }

    public static boolean isCheckStrict() {
        return isCheckStrict;
    }

    public static boolean isInitDataDel() {
        return initDataDel;
    }

    public static boolean isCheckModified() {
        return isCheckModified;
    }

    public static void setIsCheckModified(boolean isCheckModified) {
        ConfigUtil.isCheckModified = isCheckModified;
    }
}
