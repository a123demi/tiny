package org.tinygroup.metadata.checkupdate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangwy11342 on 2016/10/8.
 */
public class MetaDataInfoContext {
    private static MetaDataInfoContext metaDataInfoContext = new MetaDataInfoContext();

    //元数据表对应列表
    private List<MetaDataFileInfo> metaDataFileInfoList = new ArrayList<MetaDataFileInfo>();

    //key list
    private List<String> keys = new ArrayList<String>();

    private MetaDataInfoContext(){

    }

    public static MetaDataInfoContext getInstance(){
        return metaDataInfoContext;
    }

    public List<MetaDataFileInfo> getMetaDataFileInfoList() {
        return metaDataFileInfoList;
    }

    public void setMetaDataFileInfoList(List<MetaDataFileInfo> metaDataFileInfoList) {
        this.metaDataFileInfoList = metaDataFileInfoList;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    public void put(MetaDataFileInfo metaDataFileInfo){
        if(!metaDataFileInfoList.contains(metaDataFileInfo)) {
            metaDataFileInfoList.add(metaDataFileInfo);
            keys.add(metaDataFileInfo.getResourceId()+metaDataFileInfo.getType());
        }
    }

}
