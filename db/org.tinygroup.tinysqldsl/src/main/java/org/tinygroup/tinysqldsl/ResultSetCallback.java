package org.tinygroup.tinysqldsl;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 结果集回调处理
 * @author renhui
 *
 */
public interface ResultSetCallback {
    
	void callback(ResultSet rs) throws SQLException;
}
