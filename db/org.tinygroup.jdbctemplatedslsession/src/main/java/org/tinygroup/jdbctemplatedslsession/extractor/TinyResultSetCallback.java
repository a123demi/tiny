package org.tinygroup.jdbctemplatedslsession.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.tinygroup.tinysqldsl.ResultSetCallback;

public  class TinyResultSetCallback implements
		ResultSetExtractor<Object> {

	private ResultSetCallback callback;

	public TinyResultSetCallback(ResultSetCallback callback) {
		super();
		this.callback = callback;
	}

	public Object extractData(ResultSet rs) throws SQLException,
			DataAccessException {
		callback.callback(rs);
		return null;
	}

}
