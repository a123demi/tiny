package org.tinygroup.jdbctemplatedslsession;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.tinygroup.tinysqldsl.DslSession;

public final class DslSessionFactory {

	public static DslSession createSession(DataSource dataSource) {
		return createSession(dataSource, new DataSourceTransactionManager(
				dataSource), new DefaultTransactionDefinition());
	}

	public static DslSession createSession(DataSource dataSource,
			TransactionDefinition transactionDefinition) {
		return createSession(dataSource, new DataSourceTransactionManager(
				dataSource), transactionDefinition);
	}

	public static DslSession createSession(DataSource dataSource,
			PlatformTransactionManager transactionManager,
			TransactionDefinition transactionDefinition) {
		SimpleDslSession dslSession = new SimpleDslSession(dataSource);
		dslSession.setTransactionManager(transactionManager);
		dslSession.setTransactionDefinition(transactionDefinition);
		return dslSession;
	}
	
	public static DslSession createSessionWithNewTransaction(DataSource dataSource) {
		return createSession(dataSource, new DefaultTransactionDefinition(
				TransactionDefinition.PROPAGATION_REQUIRES_NEW));
	}

}
