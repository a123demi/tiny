package org.tinygroup.jdbctemplatedslsession.execute;

import static org.tinygroup.jdbctemplatedslsession.CustomTable.CUSTOM;
import static org.tinygroup.tinysqldsl.Delete.delete;
import static org.tinygroup.tinysqldsl.Insert.insertInto;
import static org.tinygroup.tinysqldsl.Select.selectFrom;

import org.tinygroup.jdbctemplatedslsession.DslSessionFactory;
import org.tinygroup.jdbctemplatedslsession.SimpleDslSession;
import org.tinygroup.tinysqldsl.Delete;
import org.tinygroup.tinysqldsl.DslSession;
import org.tinygroup.tinysqldsl.Insert;
import org.tinygroup.tinysqldsl.Select;

public class TransactionTest extends BaseTest {

	public void testTransaction() {

		Delete delete = delete(CUSTOM);
		DslSession session = new SimpleDslSession(dataSource);
		session.execute(delete);

		try {
			session.beginTransaction();
			Insert customInsert = insertInto(CUSTOM).values(
					CUSTOM.ID.value("10001"), CUSTOM.NAME.value("悠悠然然"),
					CUSTOM.AGE.value(22));
			session.execute(customInsert);
			session.execute(customInsert);
			session.commitTransaction();
		} catch (Exception e) {
			session.rollbackTransaction();
		}
		Select select = selectFrom(CUSTOM);
		int count = session.count(select);
		assertEquals(0, count);
	}

	public void testNestedTransaction() {
		Delete delete = delete(CUSTOM);
		DslSession session = new SimpleDslSession(dataSource);
		session.execute(delete);
		try {
			session.beginTransaction();
			Insert customInsert = insertInto(CUSTOM).values(
					CUSTOM.ID.value("10001"), CUSTOM.NAME.value("悠悠然然"),
					CUSTOM.AGE.value(22));
			session.execute(customInsert);
			DslSession newSession = DslSessionFactory
					.createSessionWithNewTransaction(dataSource);
			try {
				customInsert = insertInto(CUSTOM).values(
						CUSTOM.ID.value("10002"), CUSTOM.NAME.value("flank"),
						CUSTOM.AGE.value(23));
				newSession.beginTransaction();
				newSession.execute(customInsert);
				newSession.execute(customInsert);
				newSession.commitTransaction();
			} catch (Exception e) {
				newSession.rollbackTransaction();
			}
			session.commitTransaction();
		} catch (Exception e) {
			session.rollbackTransaction();
		}
		Select select = selectFrom(CUSTOM);
		int count = session.count(select);
		assertEquals(1, count);
	}

}
