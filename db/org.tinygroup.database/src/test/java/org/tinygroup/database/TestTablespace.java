package org.tinygroup.database;

import com.thoughtworks.xstream.XStream;
import org.tinygroup.database.config.table.Table;
import org.tinygroup.database.config.tablespace.TableSpace;
import org.tinygroup.database.config.tablespace.TableSpaces;
import org.tinygroup.database.util.DataBaseUtil;
import org.tinygroup.xstream.XStreamFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangwy11342 on 2016/8/1.
 */
public class TestTablespace{
    public static void main(String[] args) {
        XStream stream = XStreamFactory.getXStream(DataBaseUtil.DATABASE_XSTREAM);
        stream.processAnnotations(TableSpaces.class);

        TableSpaces tableSpaces = new TableSpaces();
        List<TableSpace> tableArrayList=new ArrayList<TableSpace>();
        tableSpaces.setTableSpaces(tableArrayList);
        TableSpace tableSpace = new TableSpace();
        tableSpace.setName("user1");
        tableSpace.setDescription("user1空间描述");
        tableSpace.setTitle("user1空间");
        tableSpace.setId("user1_tb_space");
        tableArrayList.add(tableSpace);

        tableSpace = new TableSpace();
        tableSpace.setName("user2");
        tableSpace.setDescription("user2空间描述");
        tableSpace.setTitle("user2空间");
        tableSpace.setId("user2_tb_space");
        tableArrayList.add(tableSpace);

        System.out.println(stream.toXML(tableSpaces));
    }
}
