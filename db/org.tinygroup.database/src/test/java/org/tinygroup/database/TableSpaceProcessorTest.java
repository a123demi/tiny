package org.tinygroup.database;

import junit.framework.TestCase;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.database.tablespace.TableSpaceProcessor;
import org.tinygroup.database.util.DataBaseUtil;

/**
 * Created by wangwy11342 on 2016/8/2.
 */
public class TableSpaceProcessorTest extends TestCase{
    TableSpaceProcessor tableSpaceProcessor;
    static {
        TestInit.init();

    }

    protected void setUp() throws Exception {
        super.setUp();
        tableSpaceProcessor = BeanContainerFactory.getBeanContainer(this.getClass().getClassLoader()).getBean(DataBaseUtil.TABLE_SPACE_PROCESSOR_BEAN);
    }

    public void testProcessor(){
        assertEquals ("ts2",tableSpaceProcessor.getTableSpace("bcd63471d4f14f83878a993efe97a109").getName());
    }

    public void testDataBaseUtil(){
        assertEquals("ts2",DataBaseUtil
                .getTableSpace(getClass().getClassLoader(),"bcd63471d4f14f83878a993efe97a109")
        .getName());
    }
}
