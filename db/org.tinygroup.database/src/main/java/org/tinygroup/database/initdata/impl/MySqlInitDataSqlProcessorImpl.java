package org.tinygroup.database.initdata.impl;

import org.tinygroup.database.config.table.Table;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangwy11342 on 2016/8/25.
 */
public class MySqlInitDataSqlProcessorImpl extends InitDataSqlProcessorImpl{
    protected String delimiter(String name) {
        return "`"+name+"`";
    }

    public List<String> getPreInitSql(List<Table> tableList) {
        List<String> list = new ArrayList<String>();
        list.add("SET foreign_key_checks = 0");
        return list;
    }

    public List<String> getPostInitSql(List<Table> tableList) throws SQLException {
        List<String> list = new ArrayList<String>();
        list.add("SET foreign_key_checks = 1");
        return list;
    }
}
