/**
 * 
 */
package org.tinygroup.database.config.tablespace;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.tinygroup.metadata.config.BaseObject;

/**
 * @author yanwj06282
 *
 */
@XStreamAlias("table-space")
public class TableSpace extends BaseObject {

}
