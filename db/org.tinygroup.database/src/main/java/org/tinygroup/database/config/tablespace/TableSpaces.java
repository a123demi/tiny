/**
 * 
 */
package org.tinygroup.database.config.tablespace;

import java.util.ArrayList;
import java.util.List;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.tinygroup.metadata.config.BaseObject;

/**
 * @author yanwj06282
 *
 */
@XStreamAlias("table-spaces")
public class TableSpaces extends BaseObject {

	@XStreamImplicit
	private List<TableSpace> tableSpaces;

	public List<TableSpace> getTableSpaces() {
		if (tableSpaces == null) {
			tableSpaces = new ArrayList<TableSpace>();
		}
		return tableSpaces;
	}

	public void setTableSpaces(List<TableSpace> tableSpaces) {
		this.tableSpaces = tableSpaces;
	}
	
}
