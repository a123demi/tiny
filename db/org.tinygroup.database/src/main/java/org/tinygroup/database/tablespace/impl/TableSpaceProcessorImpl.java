package org.tinygroup.database.tablespace.impl;

import org.tinygroup.commons.tools.CollectionUtil;
import org.tinygroup.database.ProcessorManager;
import org.tinygroup.database.config.tablespace.TableSpace;
import org.tinygroup.database.config.tablespace.TableSpaces;
import org.tinygroup.database.exception.DatabaseRuntimeException;
import org.tinygroup.database.table.TableProcessor;
import org.tinygroup.database.table.impl.TableProcessorImpl;
import org.tinygroup.database.tablespace.TableSpaceProcessor;

import java.util.HashMap;
import java.util.Map;

import static org.tinygroup.database.exception.DatabaseErrorCode.TABLESPACE__ADD_ALREADY_ERROR;

/**
 * Created by wangwy11342 on 2016/8/1.
 */
public class TableSpaceProcessorImpl implements TableSpaceProcessor {

    private ProcessorManager processorManager;

    private Map<String, TableSpace> nameMap = new HashMap<String, TableSpace>();

    private Map<String, TableSpace> idMap = new HashMap<String, TableSpace>();

    private static TableSpaceProcessor tableSpaceProcessor = new TableSpaceProcessorImpl();

    public ProcessorManager getProcessorManager() {
        return processorManager;
    }

    public void setProcessorManager(ProcessorManager processorManager) {
        this.processorManager = processorManager;
    }

    public static TableSpaceProcessor getTableSpaceProcessor() {
        return tableSpaceProcessor;
    }

    public void addTableSpaces(TableSpaces tableSpaces) {
        for (TableSpace tableSpace : tableSpaces.getTableSpaces()) {
            addTableSpace(tableSpace);
        }
    }

    public void addTableSpace(TableSpace tableSpace) {
        nameMap.put(tableSpace.getName(), tableSpace);
        if(idMap.containsKey(tableSpace.getId())){
            //重复表格异常
            throw new DatabaseRuntimeException(TABLESPACE__ADD_ALREADY_ERROR,tableSpace.getId());
        }
        idMap.put(tableSpace.getId(), tableSpace);
    }

    public TableSpace getTableSpace(String tableSpaceId) {
        if (idMap.containsKey(tableSpaceId)) {
            return idMap.get(tableSpaceId);
        }
        throw new RuntimeException(String.format("找不到ID：[%s]的表空间。", tableSpaceId));
    }

    public void removeTableSpaces(TableSpaces tableSpaces) {
        if(!CollectionUtil.isEmpty(nameMap)){
            for(TableSpace tableSpace:tableSpaces.getTableSpaces()){
                removeTableSpace(tableSpace);
            }
        }
    }

    public void removeTableSpace(TableSpace tableSpace) {
        if (!CollectionUtil.isEmpty(nameMap)) {
            nameMap.remove(tableSpace.getName());
        }
        idMap.remove(tableSpace.getId());
    }
}
