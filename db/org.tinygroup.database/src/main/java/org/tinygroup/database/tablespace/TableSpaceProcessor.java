package org.tinygroup.database.tablespace;

import org.tinygroup.database.config.tablespace.TableSpace;
import org.tinygroup.database.config.tablespace.TableSpaces;

/**
 * Created by wangwy11342 on 2016/8/1.
 */
public interface TableSpaceProcessor {
    String BEAN_NAME="tableSpaceProcessor";

    void addTableSpaces(TableSpaces tableSpaces);

    void removeTableSpaces(TableSpaces oldTableSpaces);

    void addTableSpace(TableSpace tableSpace);

    TableSpace getTableSpace(String tableSpace);
}
