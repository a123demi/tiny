package org.tinygroup.database.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wangwy11342 on 2016/8/9.
 */
public class MetadataInfos {
    private static Map<String,Integer> metadataMap = new HashMap<String, Integer>();

    public static Map<String, Integer> getInfos() {
        return metadataMap;
    }

    public static void resetInfo(String key,Integer value){
        if(metadataMap.containsKey(key)){
            metadataMap.put(key,value);
        }
    }

    public static void initMetadataMap(Map<String, Integer> map) {
        metadataMap = map;
    }


}
