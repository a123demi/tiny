package org.tinygroup.database.util;

import org.tinygroup.config.util.ConfigurationUtil;

import java.io.*;
import java.util.Properties;

/**
 * Created by wangwy11342 on 2016/8/8.
 */
public class DatabaseTmpProUtil {

    protected static final String TEMP_DIR = System.getProperty("user.home");

    protected static final String USER_DIR = new File(System.getProperty("user.dir")).getName();
    /**
     * 保存属性文件信息
     * @param proFile
     * @param tmpFileProperties 属性临时文件
     * @param newProperties 属性增量
     * @param title
     */
    public static void saveTmpFileProperties(File proFile, Properties tmpFileProperties, Properties newProperties, String title) {
        if(newProperties==null || newProperties.size()==0){
            return;
        }
        tmpFileProperties.putAll(newProperties);
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(proFile);
            tmpFileProperties.store(outputStream, title);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
            }
        }
    }

    public static Properties loadFileProperties(File proFile) {
        Properties fileProperties = new Properties();
        if (!proFile.exists()) {
            try {
                proFile.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(proFile);
            fileProperties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return fileProperties;
    }
}
