package org.tinygroup.database.tablespace.impl;

import org.tinygroup.database.config.table.Table;
import org.tinygroup.database.tablespace.TableSpaceProcessor;
import org.tinygroup.database.tablespace.TableSpaceSqlProcessor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by wangwy11342 on 2016/8/2.
 */
public abstract class TableSpaceSqlProcessorImpl implements TableSpaceSqlProcessor{
    private TableSpaceProcessor tableSpaceProcessor;


    public List<String> getCreateSql(Table table, String packageName) {
        return null;
    }

    public List<String> getUpdateSql(Table table, String packageName, Connection connection) throws SQLException {
        return null;
    }
}
