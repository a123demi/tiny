/**
 *  Copyright (c) 1997-2013, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.tinygroup.database.util;

import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.commons.namestrategy.NameStrategy;
import org.tinygroup.commons.namestrategy.impl.NormalCaseStrategy;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.database.config.table.Table;
import org.tinygroup.database.config.table.TableField;
import org.tinygroup.database.config.tablespace.TableSpace;
import org.tinygroup.database.config.view.View;
import org.tinygroup.database.table.TableProcessor;
import org.tinygroup.database.table.impl.TableProcessorImpl;
import org.tinygroup.database.tablespace.TableSpaceProcessor;
import org.tinygroup.database.tablespace.impl.TableSpaceProcessorImpl;
import org.tinygroup.database.view.ViewProcessor;
import org.tinygroup.metadata.config.stdfield.StandardField;
import org.tinygroup.metadata.util.ConfigUtil;
import org.tinygroup.metadata.util.MetadataUtil;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DataBaseUtil {
	public static String DATABASE_XSTREAM = "database";
	public static String INITDATA_XSTREAM = "initdata";
	public static String PROCESSOR_XSTREAM = "processor";
	public static String PROCESSORMANAGER_BEAN = "processorManager";
	public static String TABLEPROCESSOR_BEAN = "tableProcessor";
	public static String CUSTOMESQL_BEAN = "customSqlProcessor";
	public static String FUNCTION_BEAN = "dialectFunctionProcessor";
	public static String INITDATA_BEAN = "initDataProcessor";
	public static String PROCEDURE_BEAN = "procedureProcessor";
	public static String VIEW_BEAN = "viewProcessor";
	public static String TRIGGER_BEAN = "triggerProcessor";
	public static String SEQUENCE_BEAN = "sequenceProcessor";
	public static String TABLE_SPACE_PROCESSOR_BEAN = "tableSpaceProcessor";

	/* 数据库类型 */
	public static final String DB_TYPE_ORACLE = "oracle";
	public static final String DB_TYPE_DB2 = "db2";
	public static final String DB_TYPE_MYSQL = "mysql";
	public static final String DB_TYPE_SQLSERVER = "sqlserver";
	public static final String DB_TYPE_INFORMIX = "informix";
	public static final String DB_TYPE_SYBASE = "sybase";
	public static final String DB_TYPE_DERBY = "derby";
	public  static ThreadLocal<String> fromSourceLocal = new ThreadLocal<String>();


	public static StandardField getStandardField(String tableFieldId,
			Table table,ClassLoader loader) {
		for (TableField field : table.getFieldList()) {
			if (field.getId().equals(tableFieldId)) {
				return MetadataUtil
						.getStandardField(field.getStandardFieldId(),loader);
			}
		}
		return null;
	}

	public static Table getTableById(String id,ClassLoader loader) {
		return getTableProcessor(loader).getTableById(id);
	}

	public static List<Table> getTables(ClassLoader loader) {
		return getTableProcessor(loader).getTables();
	}

	private static TableProcessor getTableProcessor(ClassLoader loader){
		try {
			return BeanContainerFactory.getBeanContainer(loader).getBean(
					DataBaseUtil.TABLEPROCESSOR_BEAN);
		}catch (Exception e){

		}
		return TableProcessorImpl.getTableProcessor();
	}

	private static TableSpaceProcessor getTableSpaceProcessor(ClassLoader loader){
		try {
			return BeanContainerFactory.getBeanContainer(loader).getBean(
					DataBaseUtil.TABLE_SPACE_PROCESSOR_BEAN);
		}catch (Exception e){

		}
		return TableSpaceProcessorImpl.getTableSpaceProcessor();
	}

	public static TableSpace getTableSpace(ClassLoader loader,String spaceId){
		TableSpaceProcessor tableSpaceProcessor = getTableSpaceProcessor(loader);
		return tableSpaceProcessor.getTableSpace(spaceId);
	}

	public static View getViewById(String id,ClassLoader loader) {
		ViewProcessor viewProcessor = BeanContainerFactory.getBeanContainer(loader).getBean(
				DataBaseUtil.VIEW_BEAN);
		return viewProcessor.getViewById(id);
	}


	public static TableField getPrimaryField(Table table){
		for(TableField field:table.getFieldList()){
			if(field.getPrimary()) {
				return field;
			}
		}
		throw new RuntimeException("表格"+table.getName()+"主键不存在");
	}

	public static NameStrategy getNameStrategy() {
		return new NormalCaseStrategy();
	}

	public static String getDataBaseName(String name) {
		return getNameStrategy().getFieldName(name);
	}

	public static String getSchema(Table table,DatabaseMetaData metadata) throws SQLException{
		String schema = table.getSchema();
		if(schema == null ||"".equals(schema)){
			schema = metadata.getUserName();
		}
		return schema;
	}


	public static void closeResultSet(ResultSet r){
		if (r != null) {
			try {
				r.close();
			} catch (SQLException e) {
			}
		}
	}

	public static boolean isNeedCache(){
		String from = fromSourceLocal.get();
		//从processor且检查更新则需要缓存
		return ("processor".equals(from) && ConfigUtil.isCheckModified());
	}
}
