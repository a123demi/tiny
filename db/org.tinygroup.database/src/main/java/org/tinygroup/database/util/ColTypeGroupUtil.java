package org.tinygroup.database.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类型分组
 * Created by wangwy11342 on 2016/8/4.
 */
public class ColTypeGroupUtil {
    private static Map<String,String> transTypeMap = new HashMap<String,String>();

    private static List<String> dbNoLengthTypes = new ArrayList<String>();
    static {
        transTypeMap.put("INTEGER", "NUMBER");

        dbNoLengthTypes.add("NUMBER");
        dbNoLengthTypes.add("INTEGER");
 /*       dbNoLengthTypes.add("INT");
        dbNoLengthTypes.add("TINYINT");*/
    }

    public static String getSpecialType(String typeName){
        return transTypeMap.get(typeName);
    }

    public static boolean isNumberType(String typeName){
        return dbNoLengthTypes.contains(typeName);
    }
}
