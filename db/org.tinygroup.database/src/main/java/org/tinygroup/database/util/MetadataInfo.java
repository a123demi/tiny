package org.tinygroup.database.util;

/**
 * Created by wangwy11342 on 2016/8/9.
 */
public class MetadataInfo {
    private String type;

    private Integer status;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
