package org.tinygroup.database.tablespace;

import org.tinygroup.database.config.table.Table;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by wangwy11342 on 2016/8/2.
 */
public interface TableSpaceSqlProcessor {

    List<String> getCreateSql(Table table, String packageName);

    List<String> getUpdateSql(Table table, String packageName,
                              Connection connection)throws SQLException;

}
