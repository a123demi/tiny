package org.tinygroup.redis.test;

import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.jedis.JedisManager;
import org.tinygroup.jedis.config.JedisConfig;
import org.tinygroup.tinyrunner.Runner;

import junit.framework.TestCase;

/**
 * 增加默认jedis配置的设置
 * @author yancheng11334
 *
 */
public class JedisConfigTest extends TestCase {

	private JedisManager jedisManager;
	
	protected void setUp() throws Exception {
        Runner.init("application.xml", null);
        jedisManager = BeanContainerFactory.getBeanContainer(this.getClass().getClassLoader()).getBean("jedisManager");
    }
	
	public void testJedisConfig(){
		
		 JedisConfig j1 = jedisManager.getJedisConfig("server01");
		 assertEquals("testdb", j1.getHost());
		 
		 //测试默认配置
		 JedisConfig j2 = jedisManager.getJedisConfig(null);
		 assertEquals("testdb1", j2.getHost());
		 
		 j2 = jedisManager.getJedisConfig("XXXXX");
		 assertEquals(63000, j2.getPort());
	}
}
