/**
 * Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 * <p>
 * Licensed under the GPL, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tinygroup.servicewrapper.testcase;

import junit.framework.TestCase;
import org.apache.log4j.chainsaw.Main;
import org.tinygroup.beancontainer.BeanContainer;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.servicewrapper.GeneratorServiceIn;
import org.tinygroup.servicewrapper.ServiceOrg;
import org.tinygroup.servicewrapper.ServiceUser;
import org.tinygroup.tinyrunner.Runner;

import java.util.ArrayList;
import java.util.List;

public class ServiceWrapperTest extends TestCase {
    public void testInterceptor() {
        Runner.init("application.xml", new ArrayList<String>());
        BeanContainer container = BeanContainerFactory.getBeanContainer(Main.class.getClassLoader());
        GeneratorServiceIn bean = (GeneratorServiceIn) container.getBean("busiServiceProxy");
        ServiceUser user = new ServiceUser();
        user.setName("username");
        user.setAge(11);
        ServiceOrg org = new ServiceOrg();
        org.setName("hundsun");
        ServiceUser serviceUser = bean.userObject(user, org);
        assertEquals("changerName", serviceUser.getName());

        //xml
        List<ServiceUser> users = new ArrayList<ServiceUser>();
        ServiceUser xmluser = new ServiceUser();
        xmluser.setAge(100);
        xmluser.setName("lilei");
        xmluser.setMale(true);
        users.add(xmluser);
        List<ServiceUser> users2 = bean.userList(user, users);
        assertEquals(users2.get(0).getAge(), 100);
    }
}
