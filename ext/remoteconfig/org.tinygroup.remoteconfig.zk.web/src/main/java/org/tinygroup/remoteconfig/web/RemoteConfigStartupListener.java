/**
 * Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 * <p>
 * Licensed under the GPL, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tinygroup.remoteconfig.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.tinygroup.logger.LogLevel;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;
import org.tinygroup.remoteconfig.zk.manager.impl.ZKConfigClientImpl;
import org.tinygroup.weblayer.util.TinyConfigParamUtil;

public class RemoteConfigStartupListener implements ServletContextListener {
    private static Logger logger = LoggerFactory
            .getLogger(RemoteConfigStartupListener.class);

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.logMessage(LogLevel.INFO, "WEB 远程配置启动中...");

        TinyConfigParamUtil.setReadClient(new ZKConfigClientImpl());

        logger.logMessage(LogLevel.INFO, "WEB 远程配置启动完成...");
    }

}
