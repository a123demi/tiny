/**
 * Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 * <p>
 * Licensed under the GPL, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tinygroup.mmseg4j;

import com.chenlb.mmseg4j.analysis.MMSegAnalyzer;

import java.io.Reader;

/**
 * MMSegAnalyzer包装类，修正在lucene4.6以上报错的问题
 * @author yancheng11334
 *
 */
public class NewSimpleAnalyzer extends MMSegAnalyzer {

    public NewSimpleAnalyzer() {
        super();
    }

    public NewSimpleAnalyzer(String path) {
        super(path);
    }

    protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
        return new TokenStreamComponents(new NewMMSegTokenizer(newSeg(), reader));
    }
}
