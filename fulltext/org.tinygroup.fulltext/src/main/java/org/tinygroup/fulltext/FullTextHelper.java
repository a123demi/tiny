/**
 * Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 * <p>
 * Licensed under the GPL, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tinygroup.fulltext;

import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.config.util.ConfigurationUtil;
import org.tinygroup.fulltext.document.Document;
import org.tinygroup.fulltext.document.HighlightDocument;
import org.tinygroup.fulltext.exception.FullTextException;

import java.util.ArrayList;
import java.util.List;

/**
 * 全文检索全局辅助类
 * @author yancheng11334
 *
 */
public class FullTextHelper {

    private static volatile String _id = "_id";
    private static volatile String _type = "_type";
    private static volatile String _title = "_title";
    private static volatile String _abstract = "_abstract";

    /**
     * 得到默认的ID存储名
     * @return
     */
    public static String getStoreId() {
        return _id;
    }

    /**
     * 设置默认的ID存储名
     * @param id
     */
    public static void setStoreId(String id) {
        _id = id;
    }

    /**
     * 得到默认的type存储名
     * @return
     */
    public static String getStoreType() {
        return _type;
    }

    /**
     * 设置默认的type存储名
     * @param type
     */
    public static void setStoreType(String type) {
        _type = type;
    }

    /**
     * 得到默认的title存储名
     * @return
     */
    public static String getStoreTitle() {
        return _title;
    }

    /**
     * 设置默认的title存储名
     * @param type
     */
    public static void setStoreTitle(String title) {
        _title = title;
    }

    /**
     * 得到默认的abstract存储名
     * @return
     */
    public static String getStoreAbstract() {
        return _abstract;
    }

    /**
     * 设置默认的type存储名
     * @param type
     */
    public static void setStoreAbstract(String Abstract) {
        _abstract = Abstract;
    }

    /**
     * 得到FullText的实例
     * @return
     */
    public static FullText getFullText() {
        String bean = ConfigurationUtil.getConfigurationManager()
                .getConfiguration(FullText.FULLTEXT_BEAN_NAME);
        if (StringUtil.isEmpty(bean)) {
            throw new FullTextException(String.format("FullText的bean配置不存在，请检查%s全局配置参数!", FullText.FULLTEXT_BEAN_NAME));
        }
        return (FullText) BeanContainerFactory.getBeanContainer(FullTextHelper.class.getClassLoader()).getBean(bean);
    }

    /**
     * 高亮结果
     * @param pager
     * @param arguments
     * @return
     */
    public static Pager<HighlightDocument> highlight(Pager<Document> pager, Object... arguments) {
        if (pager == null) {
            return null;
        }
        List<HighlightDocument> list = new ArrayList<HighlightDocument>();
        if (pager.getRecords() != null) {
            for (Document doc : pager.getRecords()) {
                list.add(new HighlightDocument(doc, arguments));
            }
        }
        return new Pager<HighlightDocument>(pager.getTotalCount(), pager.getStart(), pager.getLimit(), list);
    }

}
