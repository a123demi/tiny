package org.tinygroup.tinytest;

import org.springframework.test.context.cache.ContextCache;
import org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate;

/**
 * 替换DefaultCacheAwareContextLoaderDelegate为的是暴露ContextCache
 * @author renhui
 *
 */
public class TinyCacheAwareContextLoaderDelegate extends
		DefaultCacheAwareContextLoaderDelegate {

    /**
     * 往外暴露 ContextCache对象
     * @return
     */
	public ContextCache getPublicContextCache() {
		return getContextCache();
	}

}
