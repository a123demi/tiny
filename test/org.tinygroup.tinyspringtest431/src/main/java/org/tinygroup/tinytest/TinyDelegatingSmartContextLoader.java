package org.tinygroup.tinytest;

import org.springframework.context.ApplicationContext;
import org.springframework.test.context.MergedContextConfiguration;
import org.springframework.test.context.SmartContextLoader;
import org.springframework.test.context.support.AbstractDelegatingSmartContextLoader;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.springutil.ExtendsSpringBeanContainer;

/**
 * 重写loadContext方法，捕捉IllegalStateException，返回GenericApplicationContext实例
 * 
 * @author renhui
 *
 */
public class TinyDelegatingSmartContextLoader extends
		AbstractDelegatingSmartContextLoader {

	private final SmartContextLoader smartContextLoader;

	public TinyDelegatingSmartContextLoader(
			SmartContextLoader smartContextLoader) {
		super();
		this.smartContextLoader = smartContextLoader;
	}

	@Override
	public ApplicationContext loadContext(
			MergedContextConfiguration mergedConfig) throws Exception {

		try {
			return smartContextLoader.loadContext(mergedConfig);
		} catch (IllegalStateException e) {
			ExtendsSpringBeanContainer beanContainer = (ExtendsSpringBeanContainer) BeanContainerFactory
					.getBeanContainer(getClass().getClassLoader());
			return beanContainer.getBeanContainerPrototype();
		}
	}

	@Override
	protected SmartContextLoader getXmlLoader() {
		return null;
	}

	@Override
	protected SmartContextLoader getAnnotationConfigLoader() {
		return null;
	}

}
