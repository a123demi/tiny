package org.tinygroup.tinytest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.runners.model.InitializationError;
import org.springframework.test.context.TestContextBootstrapper;
import org.springframework.test.context.TestContextManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

public class TinyJUnit4ClassRunner extends SpringJUnit4ClassRunner {
	
	private static final Log LOGGER = LogFactory.getLog(TinyJUnit4ClassRunner.class);

	public TinyJUnit4ClassRunner(Class<?> clazz) throws InitializationError {
		super(clazz);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("TinyJUnit4ClassRunner constructor called with [" + clazz + "]");
		}
		
	}
	
	protected TestContextManager createTestContextManager(Class<?> testClass) {
		TestContextBootstrapper testContextBootstrapper = BootstrapUtils.resolveTestContextBootstrapper(BootstrapUtils.createBootstrapContext(testClass));
		return new TestContextManager(testContextBootstrapper);
	}

}
