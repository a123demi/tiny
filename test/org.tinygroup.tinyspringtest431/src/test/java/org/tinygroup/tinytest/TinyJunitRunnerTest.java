package org.tinygroup.tinytest;

import static org.junit.Assert.assertEquals;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.transaction.annotation.Transactional;

@RunWith(TinyJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test.xml")
@Transactional
public class TinyJunitRunnerTest extends BaseTest {

	@Resource(name="derbyDataSource")
	private DataSource derbyDataSource;
	private JdbcTemplate jdbcTemplate;
	
	@Value(value="${BASE_PACKAGE}")
	private String value;
	
	
	@Before
	public void before(){
		jdbcTemplate=new JdbcTemplate(derbyDataSource);
	}
	
	@AfterTransaction
	public void after(){
		int count = jdbcTemplate.queryForObject("select count(*) from custom",
				Integer.class);
		assertEquals(0, count);
	}
	

	@Test
	public void testTransaction() {
		assertEquals("org.hundsun", value);
		jdbcTemplate.execute("insert into custom(id,name,age) values(10,'sds',16)");
		jdbcTemplate.execute("insert into custom(id,name,age) values(11,'sds',16)");
		int count=jdbcTemplate.queryForObject("select count(*) from custom", Integer.class);
		assertEquals(2, count);
	}

}
