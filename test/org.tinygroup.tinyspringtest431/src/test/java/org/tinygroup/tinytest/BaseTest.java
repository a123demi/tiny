package org.tinygroup.tinytest;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.BeforeClass;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.tinygroup.commons.tools.Resources;
import org.tinygroup.tinytestutil.script.ScriptRunner;

public abstract class BaseTest extends AbstractTransactionalJUnit4SpringContextTests {

	@BeforeClass
	public static void setUp() throws Exception {
		Connection conn = null;
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			conn = DriverManager.getConnection("jdbc:derby:TESTDB;create=true", "opensource", "opensource");
			ScriptRunner debyrunner = new ScriptRunner(conn, false, false);
			Resources.setCharset(Charset.forName("utf-8"));
			debyrunner.runScript(Resources
					.getResourceAsReader("table_derby.sql"));

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

	}

}
