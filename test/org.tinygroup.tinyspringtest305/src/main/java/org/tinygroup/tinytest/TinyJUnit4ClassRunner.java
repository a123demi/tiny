package org.tinygroup.tinytest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.runners.model.InitializationError;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tinygroup.tinytest.contextloader.TinyGenericXmlContextLoader;

public class TinyJUnit4ClassRunner extends SpringJUnit4ClassRunner {

	private static final Log LOGGER = LogFactory
			.getLog(TinyJUnit4ClassRunner.class);

	public TinyJUnit4ClassRunner(Class<?> clazz) throws InitializationError {
		super(clazz);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("TinyJUnit4ClassRunner constructor called with ["
					+ clazz + "]");
		}
	}

	@Override
	protected String getDefaultContextLoaderClassName(Class<?> clazz) {
		return TinyGenericXmlContextLoader.class.getName();
	}

}
