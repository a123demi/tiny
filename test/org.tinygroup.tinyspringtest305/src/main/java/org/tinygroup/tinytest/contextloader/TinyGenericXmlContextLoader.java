package org.tinygroup.tinytest.contextloader;

import java.util.ArrayList;

import org.springframework.beans.factory.support.BeanDefinitionReader;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.springutil.ExtendsSpringBeanContainer;
import org.tinygroup.tinyrunner.Runner;

/**
 * 
 * @author renhui
 *
 */
public class TinyGenericXmlContextLoader extends AbstractGenericContextLoader {

	/**
	 * Create a new {@link XmlBeanDefinitionReader}.
	 * 
	 * @return a new XmlBeanDefinitionReader.
	 * @see XmlBeanDefinitionReader
	 */
	@Override
	protected BeanDefinitionReader createBeanDefinitionReader(
			final GenericApplicationContext context) {
		return new XmlBeanDefinitionReader(context);
	}

	/**
	 * Returns &quot;<code>-context.xml</code>&quot;.
	 */
	@Override
	public String getResourceSuffix() {
		return "-context.xml";
	}

	@Override
	protected void prepareContext(GenericApplicationContext context) {
		// 启动tiny框架
		FileSystemXmlApplicationContext applicationContext = new FileSystemXmlApplicationContext();
		BeanContainerFactory.initBeanContainer(ExtendsSpringBeanContainer.class
				.getName());
		ExtendsSpringBeanContainer beanContainer = (ExtendsSpringBeanContainer) BeanContainerFactory
				.getBeanContainer(getClass().getClassLoader());
		beanContainer.setApplicationContext(applicationContext);
		Runner.init(ExtendsSpringBeanContainer.class, null,
				new ArrayList<String>());
		context.setParent(applicationContext);
	}

	protected void postRefresh(GenericApplicationContext context) {
	}

}