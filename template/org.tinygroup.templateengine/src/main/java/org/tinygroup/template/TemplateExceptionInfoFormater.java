package org.tinygroup.template;

/**
 * 模板信息异常格式化接口
 * @author yancheng11334
 *
 */
public interface TemplateExceptionInfoFormater {

	String getMessage(TemplateExceptionInfo info);
}
