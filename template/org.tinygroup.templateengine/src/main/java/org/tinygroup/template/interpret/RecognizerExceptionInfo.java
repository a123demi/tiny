package org.tinygroup.template.interpret;

import org.antlr.v4.runtime.Recognizer;
import org.tinygroup.template.Macro;
import org.tinygroup.template.TemplateExceptionInfo;
import org.tinygroup.template.listener.Point;

/**
 * 处理Recognizer的异常情况
 * @author yancheng11334
 *
 */
public class RecognizerExceptionInfo implements TemplateExceptionInfo{

	private String reason;
	private String fileInfo;
	private Point start;
	
	public RecognizerExceptionInfo(String reason, String fileInfo,int line,int charPositionInLine) {
		super();
		this.reason = reason;
		this.fileInfo = fileInfo;
		this.start = new Point(charPositionInLine,line);
	}

	public String getFileInfo() {
		return fileInfo;
	}

	public String getReason() {
		return reason;
	}

	public Point getStart() {
		return start;
	}

	public Point getEnd() {
		return null;
	}

	public String getText() {
		return null;
	}

	public boolean isMacroException() {
		return false;
	}

	public Macro getMacroInfo() {
		return null;
	}

}
