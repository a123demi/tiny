package org.tinygroup.template.interpret;

/**
 * 短路异常
 * @author yancheng11334
 *
 */
public class CompareConditionException extends Exception {

	private Boolean tag;
	
	public CompareConditionException(Boolean value){
		this.tag = value;
	}

	public Boolean getTag() {
		return tag;
	}
	
}
