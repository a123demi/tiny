package org.tinygroup.template.interpret;

import org.antlr.v4.runtime.ParserRuleContext;
import org.tinygroup.template.Macro;

/**
 * 宏异常信息块
 * @author yancheng11334
 *
 */
public class MacroExceptionInfo extends AbstractTemplateExceptionInfo{

	public MacroExceptionInfo(ParserRuleContext parserRuleContext,
			String reason, String fileInfo,Macro macro){
		super(parserRuleContext,reason,fileInfo);
		this.macro = macro;
	}
	
	private Macro macro;
	
	public boolean isMacroException() {
		return true;
	}

	public Macro getMacroInfo() {
		return macro;
	}

}
