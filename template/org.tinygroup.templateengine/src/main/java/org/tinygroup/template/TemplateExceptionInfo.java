package org.tinygroup.template;

import org.tinygroup.template.listener.Point;

/**
 * 模板引擎异常信息块(一个异常会包含多个信息块)
 * @author yancheng11334
 *
 */
public interface TemplateExceptionInfo {

	/**
	 * 报错文件信息
	 * @return
	 */
	String getFileInfo();
	
	/**
	 * 报错原因
	 * @return
	 */
	String getReason();
	
	/**
	 * 报错开始位置
	 * @return
	 */
	Point  getStart();
	
	/**
	 * 报错结束位置
	 * @return
	 */
	Point  getEnd();
	
	/**
	 * 报错文本
	 * @return
	 */
	String getText();
	
	/**
	 * 是否宏异常
	 * @return
	 */
	boolean isMacroException();
	
	/**
	 * 获得宏信息
	 * @return
	 */
	Macro getMacroInfo();
}
