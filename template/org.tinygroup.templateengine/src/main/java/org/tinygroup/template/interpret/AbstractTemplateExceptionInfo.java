package org.tinygroup.template.interpret;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.tinygroup.template.TemplateExceptionInfo;
import org.tinygroup.template.listener.Point;

/**
 * 抽象的模板异常信息块
 * @author yancheng11334
 *
 */
public abstract class AbstractTemplateExceptionInfo implements TemplateExceptionInfo{

	private transient ParserRuleContext  parserRuleContext;
	private String reason;
	private String fileInfo;
	
	public AbstractTemplateExceptionInfo(ParserRuleContext parserRuleContext,
			String reason, String fileInfo) {
		super();
		this.parserRuleContext = parserRuleContext;
		this.reason = reason;
		this.fileInfo = fileInfo;
	}

	public String getFileInfo() {
		return fileInfo;
	}

	public String getReason() {
		return reason;
	}

	public Point getStart() {
		Token token = parserRuleContext.getStart();
		Point p = new Point(token.getCharPositionInLine(),token.getLine());
		return p;
	}

	public Point getEnd() {
		Token token = parserRuleContext.getStop();
		Point p = new Point(token.getCharPositionInLine(),token.getLine());
		return p;
	}

	public String getText() {
		return parserRuleContext.getText();
	}
	
	public ParserRuleContext getParserRuleContext(){
		return parserRuleContext;
	}
	
	public void updateParserRuleContext(ParserRuleContext context){
		this.parserRuleContext = context;
	}

}
