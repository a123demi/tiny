package org.tinygroup.template.interpret;

import org.apache.commons.lang.StringUtils;
import org.tinygroup.template.TemplateExceptionInfo;

/**
 * 简单格式错误的默认实现输出
 * @author yancheng11334
 *
 */
public class DefaultExceptionInfoFormater extends SimpleExceptionInfoFormater{

	protected void buildFileInfo(TemplateExceptionInfo info, StringBuilder sb) {
		if(info.getFileInfo()!=null){
			if(info.isMacroException()){
				sb.append("报错宏文件路径:").append(info.getFileInfo()).append(" 报错宏名称:").append(info.getMacroInfo().getName());   
			}else{
				sb.append("报错文件路径:").append(info.getFileInfo());
			}
			sb.append("\n");
		}
		
	}

	protected void buildReason(TemplateExceptionInfo info, StringBuilder sb) {
		if(info.getReason()!=null){
		   sb.append("报错原因:").append(info.getReason()).append("\n");
		}
		
	}

	protected void buildPlace(TemplateExceptionInfo info, StringBuilder sb) {
		if(info.getStart()!=null){
			sb.append("报错位置:[").append(info.getStart().getY()).append(",").append(info.getStart().getX()).append("]");
			if(info.getEnd()!=null){
			   sb.append("-[").append(info.getEnd().getY()).append(",").append(info.getEnd().getX()).append("]");
			}
			sb.append("\n");
		}
		
		
	}

	protected void buildText(TemplateExceptionInfo info, StringBuilder sb) {
		if(info.getText()!=null){
		   sb.append(StringUtils.leftPad("", 20, "=")).append("\n");  //分隔线
		   sb.append(info.getText()).append("\n");
		   sb.append(StringUtils.leftPad("", 20, "=")).append("\n\n");  //分隔
		}
	}

}
