package org.tinygroup.template.interpret;

import org.tinygroup.template.TemplateExceptionInfo;
import org.tinygroup.template.TemplateExceptionInfoFormater;

/**
 * 简单异常格式实现
 * @author yancheng11334
 *
 */
public abstract class SimpleExceptionInfoFormater implements TemplateExceptionInfoFormater{

	public String getMessage(TemplateExceptionInfo info) {
		StringBuilder sb = new StringBuilder();
		buildFileInfo(info,sb);
		buildReason(info,sb);
		buildPlace(info,sb);
		buildText(info,sb);
		return sb.toString();
	}
	
	/**
	 * 实现报错文件路径输出
	 * @param info
	 * @param sb
	 */
	protected abstract void buildFileInfo(TemplateExceptionInfo info,StringBuilder sb);
	
	/**
	 * 实现报错原因输出
	 * @param info
	 * @param sb
	 */
	protected abstract void buildReason(TemplateExceptionInfo info,StringBuilder sb);
	
	/**
	 * 实现报错位置输出
	 * @param info
	 * @param sb
	 */
	protected abstract void buildPlace(TemplateExceptionInfo info,StringBuilder sb);
	
	/**
	 * 实现报错内容输出
	 * @param info
	 * @param sb
	 */
	protected abstract void buildText(TemplateExceptionInfo info,StringBuilder sb);

}
