package org.tinygroup.template.interpret;

import org.antlr.v4.runtime.ParserRuleContext;
import org.tinygroup.template.Macro;

/**
 * 默认的异常信息块
 * @author yancheng11334
 *
 */
public class DefaultExceptionInfo extends AbstractTemplateExceptionInfo{

	public DefaultExceptionInfo(ParserRuleContext parserRuleContext,
			String reason, String fileInfo){
		super(parserRuleContext,reason,fileInfo);
	}
	
	public boolean isMacroException() {
		return false;
	}

	public Macro getMacroInfo() {
		return null;
	}

}
