package org.tinygroup.flow.util;

import java.util.List;

import org.tinygroup.config.impl.AbstractConfiguration;
import org.tinygroup.config.util.ConfigurationUtil;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.parser.filter.NameFilter;
import org.tinygroup.xmlparser.node.XmlNode;

public class FlowUtilConfig extends AbstractConfiguration {

	private static final String FLOW_EL_UTIL_CONFIG_PATH = "/application/el-util-config";

	public String getApplicationNodePath() {
		return FLOW_EL_UTIL_CONFIG_PATH;
	}

	public String getComponentConfigPath() {
		return "";
	}

	public void config(XmlNode applicationConfig, XmlNode componentConfig) {
		super.config(applicationConfig, componentConfig);
		XmlNode combineNode = ConfigurationUtil.combineXmlNode(
				applicationConfig, componentConfig);
		init(combineNode);
	}

	public void init(XmlNode config) {
		if (config == null) {
			LOGGER.logMessage(LogLevel.INFO, "工具类配置信息为空");
			return;
		}
		LOGGER.logMessage(LogLevel.INFO, "开始初始化工具类配置");
		NameFilter<XmlNode> nameFilter = new NameFilter<XmlNode>(config);
		List<XmlNode> aopList = nameFilter.findNodeList("el-util");
		for (XmlNode node : aopList) {
			String name = node.getAttribute("name");
			String classPath = node.getAttribute("class-path");
			Class<?> clazz;
			try {
				clazz = Class.forName(classPath);
			} catch (ClassNotFoundException e) {
				LOGGER.logMessage(LogLevel.ERROR, "初始化工具类失败：{0}", classPath);
				continue;
			}
			FlowElUtil.putUtilClass(name, clazz);
			
		}
		LOGGER.logMessage(LogLevel.INFO, "初始化工具类配置完成");

	}

}
