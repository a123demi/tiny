package org.tinygroup.flow.text.util.testcase;

import org.tinygroup.context.Context;
import org.tinygroup.context.util.ContextFactory;
import org.tinygroup.event.Event;
import org.tinygroup.flow.component.AbstractFlowComponent;

public class FlowElUtilTest extends AbstractFlowComponent {

	public void testElUtil1() {
		Context context = ContextFactory.getContext();
		context.put("a", "");
		context.put("el", "b=StringUtil.isBlank(a)");
		Event e = Event.createEvent("elUtilTest", context);
		cepcore.process(e);
		assertEquals(true, e.getServiceRequest().getContext().get("b"));
	}

	public void testElUtil2() {
		Context context = ContextFactory.getContext();
		context.put("a", "上山打老虎");
		context.put("el", "FlowTestUtil.systemout(a)");
		Event e1 = Event.createEvent("elUtilTest", context);
		cepcore.process(e1);
	}
	
	public void testElUtil3() {
		Context context = ContextFactory.getContext();
		context.put("a", "上山打老虎");
		context.put("el", "b=FlowTestUtil.getName(a)");
		Event e2 = Event.createEvent("elUtilTest", context);
		cepcore.process(e2);
		assertEquals("hello：上山打老虎", e2.getServiceRequest().getContext().get("b"));
	}
}
